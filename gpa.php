<html>
<head>
<title>GPA Calculator</title>  

<style>
body {
    text-align: center;
    background-color: #777777;
    }

</style>
</head>

<body>

<?php

If (!$_POST['submit']) {
 echo '<h1>How many grades do you want to calculate?</h1>
     ';
 echo '<h2>Enter grades for up to 9 classes</h2>
     ';
 echo '<form action="gpa.php" method="post" name="form1">
     ';
 echo '<input type="text" name="numclasses" size="2" maxlength="1">
     ';
 echo '<input type="submit" name="submit" value="submit">
     ';
 echo '</form>
     ';
} else {
    if ($_POST['numclasses']>0) {
        $numclasses = $rows = $_POST['numclasses'];
        echo '<h1>Please select a grade for all ' . $numclasses . ' classes.</h1>';
        ?>
        <form action="gpa.php" method="post" name="form2">
        <table border="2" cellpadding="2" cellspacing="2" align="center" width="300">
        <tr>
            <td>Class #</td>
            <td>Letter Grade</td>
        </tr>
        <?php
            for ($r = 1; $r <= $rows; $r++) {
            echo '<tr>
                    ';
            echo '<td>Class #' . $r . '</td>
                    ';
            echo '<td>Grade:
                  <select name="class_'.$r.'">
                    <option value="4.0">A</option>
                    <option value="3.0">B</option>
                    <option value="2.0">C</option>
                    <option value="1.0">D</option>
                    <option value="0.0">F</option>
                  </select>';
            echo '</tr>
                ';
            }
         ?>
        <tr>
            <td colspan="2" align="center"><input type="submit" name="submit" value="submit"></td>
        </tr>
        </table>
            <input type="hidden" name="final_numclasses" value="<?php echo $numclasses; ?>">
        </form>
        <?php
    }
    elseif ($_POST['numclasses']<=0 && !$_POST['class_1']) {
        echo '<h1>Please enter a number between 1 and 9</h1>
            ';
        echo '<form action="gpa.php" method="post" name="form1">
            ';
        echo '<input type="text" name="numclasses" size="2" maxlength="2">
            ';
        echo '<input type="submit" name="submit" value="submit">
            ';
        echo '</form>
            ';
    } else {
        $numclasses = $_POST['final_numclasses'];
        $c1 = $_POST['class_1'];
        $c2 = $_POST['class_2'];
        $c3 = $_POST['class_3'];
        $c4 = $_POST['class_4'];
        $c5 = $_POST['class_5'];
        $c6 = $_POST['class_6'];
        $c7 = $_POST['class_7'];
        $c8 = $_POST['class_8'];
        $c9 = $_POST['class_9'];       
        
        $gpa = ($c1+$c2+$c3+$c4+$c5+$c6+$c7+$c8+$c9)/$numclasses;
        
        $format_gpa = number_format($gpa, 2);
        
        echo '<h1>Your GPA is <b><em>'.$format_gpa.'</em></b></h1>';
    }
}
  
    
?>
    
</body>

</html>